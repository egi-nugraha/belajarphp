<?php
	// Pengkondisian
	// If Else
	// If .. Else If .. Else
	// Switch Case Break
	// Ternary
	
	/*
	$x = 10;
	if ( $x <= 20 )
	{
		echo "Benar";
	}else{
		echo "Salah";
	}
	*/
	
	/*
	if ( $x <= 20 ):
		echo "Benar";
	endif;
	*/
	
	$x = 100;
	if($x < 20 || $x == 100){
		echo "Benar";
	}else if( $x == 20 ){
		echo "Binggo !";
	}else{
		echo "Salah";
	}
?>