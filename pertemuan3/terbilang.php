<?php
	// 135
	// result : seratus tiga puluh lima ribu.
	
	// Rekursif ; Pemanggilan Dirinya Sendiri
	function terbilang($rupiah){
		$angka = array();
		$angka = ["", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"];
		
		if ($rupiah < 12)
			return " " . $angka[$rupiah];
		elseif ($rupiah < 20)
			return terbilang($rupiah - 10) . " belas";
		elseif ($rupiah < 100)
			return terbilang($rupiah / 10) . " puluh" . terbilang($rupiah % 10);
	}
	
	echo terbilang(97);
?>