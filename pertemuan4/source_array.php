<?php
	// Array
	// Variabel yang dapat memiliki banyak nilai
	// Sample #1 Array Hari Mon-Sun
	$hari1="Senin";
	$hari2="Selasa";
	
	// Cara Lama vs Cara Baru
	// $hari = array(); vs $hari = [];
	$hari = array("Senin", "Selasa");
	$bulan = ["Januari", "Februari"];
	
	// Elemen pada array boleh memiliki tipe data yang berbeda
	$arr = [123, "string", false];
	
	// Cara menampilkan seluruh array, echo(error), var_dump, print_r
	//echo $hari;
	var_dump($hari);
	echo "<br>";
	print_r($hari);
	echo "<br><br>";
	// Pasangan antara key dan value
	// Key-nya adalah index, yang dimulai dari 0
	
	// Menampilkan 1 elemen array
	echo $hari[0];
	echo "<br>";
	echo $bulan[1];
	
	// Menambahkan elemen baru pada array
	$hari[] = "Rabu"; 
	echo "<br><br>";
	
	// Pengulangan pada array (for, foreach)
	$angka = [2,3,5,11,67,123,500];
	for ( $i = 0; $i < count($angka); $i++){
		echo $angka[$i];
	}
	echo "<br>";
	foreach( $angka as $a){
		echo $a;
	}
	echo "<br><br>";
	
	// Array Dua Data (ex. Data Siswa)
	$siswa = [
		["11700169", "DIMAS IKHSAN RAMDANI", "XI RPL 1", "L"] ,
		["11700365", "MOCHAMAD SUDRAJAT", "XI RPL 1", "L"] , 
		["11700484", "REGINA GUSTIARTI", "XI RPL 1", "P"]
	];
	
	foreach($siswa as $s):
		echo $s[0];
		echo $s[1];
		echo $s[2];
		echo $s[3];
		echo "<br>";
	endforeach;
	echo "<br><br>";
	
	// Associative Array = Sama seperti array, kecuali
	// key-nya adalah string yang kita buat sendiri
	// Sample (Data Siswa)
	// Multidimensional Array array yang memiliki satu atau lebih array di dalamnya.
	// Bahkan tak terhingga / banyak
	
	
	$siswa = [
		[
			"nis" => "11700169", 
			"nama" => "DIMAS IKHSAN RAMDANI",
			"kelas" => "XI RPL 1",
			"jk" => "L"
		],
		[
			"nis" => "11700484", 
			"nama" => "REGINA GUSTIARTI",
			"kelas" => "XI RPL 1",
			"jk" => "P",
			"nilai" => [
				"quis" => [80, 70, 90], 
				"uts" => 75, 
				"uas" => 90
			]
		]
	];
	
	echo $siswa[1]["nama"];
	echo "<br>";
	echo $siswa[1]["nilai"]["quis"][1];
	echo "<br><br>";
?>