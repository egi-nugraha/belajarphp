<?php
	// Komentar 1 Baris 
	
	/*
		Komentar Baris ke-1
		Komentar Baris ke-2
		Atau Lebih dari 1 baris
	*/
	
	// Menampilkan String Selamat Datang.
	// echo "Selamat Datang, Admin";
	
	// Standar Output
	// echo untuk menampilkan value
	// print untuk menampilkan value
	// print_r() Untuk mengetahui Array
	// var_dump() Untuk mengetahui tipe data Value
	// var_dump("Selamat Datang, User");
	
	// Perbedaan penggunaan kutip (' , ").
	// echo "Jum'at";
	
	// Variabel(Alias) = Kata Kunci untuk menampilkan sebuah data
	// Syarat Penamaan : Hanya (_), Abjad dan Angka. 
	// Tidak boleh diawali dengan angka 
	// $nama = "Egi Nugraha";
	// echo "Selamat Datang, $nama";
	
	// Operator Aritmatika (+, -, /, *, %)
	// Modulus - untuk mencari sisa bagi (Ganjil atau Genap)
	// $angka = 8;
	// $mod = 2;
	// $hasil = $angka % $mod;
	// echo $hasil;
	
	// Materi yang belum tersampaikan
	/* 
		Penggabungan String, (.)
		Assigment, (=, +=, -=, *=, .=)
		Perbandingan, (<, >, <=, >= )
		Identitas, ( ===, !== )
		Logika pada PHP ( &&, ||, ! )
	*/
	
	// Penggabungan String (.) titik; Duniailkom.com
	// $nama_depan = "Egi";
	// $nama_belakang = "Nugraha";
	// $nama_full = $nama_depan . " " . $nama_belakang;
	// echo $nama_full;
	
	// Assigment String
	// $nama = "Egi";
	// $nama .= " ";
	// $nama .= "Nugraha";
	// echo $nama;
	
	// $angka = 10;
	// $angka += 5;
	// echo $angka;
	
	// Increment & Decrement (biasanya dipakai di looping)
	// $angka = 10;
	// $angka++; 
	// $angka--; 
	// echo $angka;
	
	// Perbandingan value menghasilkan boolean < > <= >=
	// True = 1, False = null[kosong];
	// nol(bernilai) vs null(kosong)
	
	// $angka = 5;
	// echo $angka == "5";
	
	// Identitas Perbandingan Melihat Tipe Data
	// $angka = 5;
	// echo $angka === "5";
	
	// Logika OR(||), AND(&&), NOT(!)
	// && Perbandingan harus terpenuhi semua
	// || Perbandingan minimal salah satu terpenuhi
	// $angka = 10;
	// var_dump($angka <= 20 || $angka % 2 == 1);
?>
















