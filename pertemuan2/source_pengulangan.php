<?php
	// Materi Pertemuan 2 - Basic PHP
	// Pengulangan
	// Kalau cukup waktu : Function , menggunakan fungsi yang ada. Membuat fungsi pada PHP.
	
	// for (inisialisasi; kondisi terminasi untuk memberhentikan perulangan; increment / decrement
	// while () sama dengan for cuma beda posisi dari 3 bagian.
	// do {} while () lakukan dulu baru cek kondisi.
	
	/*
		for( $i = 0; $i < 5; $i++ ){
			echo "Hello World! <br>";
		}
	*/
	
	/*
		$i = 0;
		while( $i < 5 ){
			echo "Hello World! <br>";
			$i++;
		}
	*/
	
	/*
		$i = 0;
		do {
			echo "Hello World! <br>";
			$i++;
		} while( $i < 5 );
	*/
	
	// Lakukan latihan membuat perulangan di tabel di HTML.
	// Sample dilakukan didalam <?php 
	// Sample dilakukan di file HTML 
	// Arti dari fungsi endfor;
	
	// Test Project Looping
?>