<?php
	// Looping (Perulangan)
	// For, While, Do .. While, Foreach
	
	// for($i=1; $i <= 5; $i++){
		// echo $i;
		// echo "Hello World";
		// echo "<br />";
	// }
	
	/*
	$i = 1;
	while($i <= 5){
		echo "Hello World !";
		echo "<br>";
		$i++;
	}
	*/
	
	/*
	$j = 1;
	do{
		echo "Hai World !";
		echo "<br>";
		$j++;
	}while($j <= 5);
	*/
	
	// Looping didalam looping
	for($i=1; $i <= 10; $i++){
		for($j = $i; $j <= 10; $j++){
			echo "*";
		}
		echo "<br>";
	}
?>